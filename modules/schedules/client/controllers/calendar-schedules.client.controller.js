(function () {
  'use strict';

  angular
    .module('schedules')
    .controller('CalendarScheduleController', CalendarScheduleController);

  CalendarScheduleController.$inject = ['$scope', 'uiCalendarConfig', '$compile'];

  function CalendarScheduleController($scope, uiCalendarConfig, $compile) {
    var vm = this;

    //Calendar
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getYear();
    
    // Calendar Functions

    //Day Click
    $scope.dayClicked = function(date, jsEvent, view){
        console.log('Clicked on: ' + date.format());
        console.log('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        console.log('Current view: ' + view.name); 
    };

    //Event Render
    $scope.eventRender = function(event, element, view){
	    element.attr({
	    	'tooltip' : event.title,
	    	'tooltip-append-to-body' : true 
	    });
	    $compile(element)($scope);
    };

    //Setup Events
    $scope.events = [
    {
    	title: 'All Day Event',
    	start: new Date(y, m, 1)
    }];

    /* event source that pulls from google.com */
    $scope.eventSource = { 
    	url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic", 
    	className: 'gcal-event', 
    	currentTimezone: 'America/Chicago'
    };


    $scope.uiConfig = {
      calendar:{
        height: 450,
        editable: true,
        themeButtonIcons: false,
        header:{
          left: 'month basicWeek basicDay agendaWeek agendaDay',
          center: 'title',
          right: 'today prev,next'
        },
        // eventClick: $scope.alertOnEventClick,
        // eventDrop: $scope.alertOnDrop,
        // eventResize: $scope.alertOnResize,
        eventRender: $scope.eventRender,
        dayClick: $scope.dayClicked
      }
    };

    $scope.eventSources = [$scope.events, $scope.eventSource];
  }

})();
