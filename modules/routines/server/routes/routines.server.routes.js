'use strict';

/**
 * Module dependencies
 */
var routinesPolicy = require('../policies/routines.server.policy'),
  routines = require('../controllers/routines.server.controller');

module.exports = function(app) {
  // Routines Routes
  app.route('/api/routines').all(routinesPolicy.isAllowed)
    .get(routines.list)
    .post(routines.create);

  app.route('/api/routines/:routineId').all(routinesPolicy.isAllowed)
    .get(routines.read)
    .put(routines.update)
    .delete(routines.delete);

  // Finish by binding the Routine middleware
  app.param('routineId', routines.routineByID);
};
