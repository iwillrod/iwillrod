'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Routine = mongoose.model('Routine'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Routine
 */
exports.create = function(req, res) {
  var routine = new Routine(req.body);
  routine.user = req.user;
  // routine.exercise = req.body.exercises;

  routine.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(routine);
    }
  });
};

/**
 * Show the current Routine
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var routine = req.routine ? req.routine.toJSON() : {};

  // Add a custom field to the Routine, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  routine.isCurrentUserOwner = req.user && routine.user && routine.user._id.toString() === req.user._id.toString() ? true : false;

  res.jsonp(routine);
};

/**
 * Update a Routine
 */
exports.update = function(req, res) {
  var routine = req.routine ;
  // var exercises = req.body.exercise;
  // console.log('routines: ' + exercises[0].name);
  routine = _.extend(routine, req.body);

  routine.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(routine);
    }
  });
};

/**
 * Delete an Routine
 */
exports.delete = function(req, res) {
  var routine = req.routine ;

  routine.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(routine);
    }
  });
};

/**
 * List of Routines
 */
exports.list = function(req, res) { 
  Routine.find().sort('-created').populate('user', 'displayName').populate('exercise').exec(function(err, routines) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(routines);
    }
  });
};

/**
 * Routine middleware
 */
exports.routineByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Routine is invalid'
    });
  }

  Routine.findById(id).populate('user', 'displayName').populate('exercise').exec(function (err, routine) {
    if (err) {
      return next(err);
    } else if (!routine) {
      return res.status(404).send({
        message: 'No Routine with that identifier has been found'
      });
    }
    req.routine = routine;
    next();
  });
};
