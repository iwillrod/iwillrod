'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Routine Schema
 */
var RoutineSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Routine name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  exercise:[{
    type: Schema.ObjectId,
    ref: 'Exercise'
  }]
});

mongoose.model('Routine', RoutineSchema);
