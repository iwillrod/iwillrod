(function () {
  'use strict';

  angular
    .module('routines')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('routines', {
        abstract: true,
        url: '/routines',
        template: '<ui-view/>'
      })
      .state('routines.list', {
        url: '',
        templateUrl: 'modules/routines/client/views/list-routines.client.view.html',
        controller: 'RoutinesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Routines List'
        }
      })
      .state('routines.create', {
        url: '/create',
        templateUrl: 'modules/routines/client/views/form-routine.client.view.html',
        controller: 'RoutinesController',
        controllerAs: 'vm',
        resolve: {
          routineResolve: newRoutine
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle : 'Routines Create'
        }
      })
      .state('routines.edit', {
        url: '/:routineId/edit',
        templateUrl: 'modules/routines/client/views/form-routine.client.view.html',
        controller: 'RoutinesController',
        controllerAs: 'vm',
        resolve: {
          routineResolve: getRoutine
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Routine {{ routineResolve.name }}'
        }
      })
      .state('routines.view', {
        url: '/:routineId',
        templateUrl: 'modules/routines/client/views/view-routine.client.view.html',
        controller: 'RoutinesController',
        controllerAs: 'vm',
        resolve: {
          routineResolve: getRoutine
        },
        data:{
          pageTitle: 'Routine {{ articleResolve.name }}'
        }
      });
  }

  getRoutine.$inject = ['$stateParams', 'RoutinesService'];

  function getRoutine($stateParams, RoutinesService) {
    return RoutinesService.get({
      routineId: $stateParams.routineId
    }).$promise;
  }

  newRoutine.$inject = ['RoutinesService'];

  function newRoutine(RoutinesService) {
    return new RoutinesService();
  }
})();
