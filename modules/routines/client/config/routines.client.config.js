(function () {
  'use strict';

  angular
    .module('routines')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Routines',
      state: 'routines',
      type: 'dropdown',
      roles: ['*'],
      position: 2
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'routines', {
      title: 'List Routines',
      state: 'routines.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'routines', {
      title: 'Create Routine',
      state: 'routines.create',
      roles: ['user']
    });
  }
})();
