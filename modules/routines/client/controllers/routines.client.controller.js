(function () {
  'use strict';

  // Routines controller
  angular
    .module('routines')
    .controller('RoutinesController', RoutinesController);

  RoutinesController.$inject = ['$scope', '$state', 'Authentication', 'routineResolve', 'ExercisesService'];

  function RoutinesController ($scope, $state, Authentication, routine, ExercisesService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.routine = routine;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    vm.moveItem = moveItem;
    vm.moveAll = moveAll;
    var currentState = $state.current;

    function checkForState(value){
      return currentState.name.indexOf(value); 
    }

    // Get all exercises based on the state we are currently in
    function init(){
      if(checkForState('create') !== -1){
        //If it's edit. Then we are dealing with an existing routine and we have Object IDs. Lets try one first
        vm.exerciseType = [];
        vm.routine.exercise = [];
        vm.exerciseType = ExercisesService.query();
      }else if(checkForState('edit') !== -1 ){
        vm.exerciseType = [];
        // vm.exerciseType = ExercisesService.query().$promise;
        ExercisesService.query().$promise.then(function(exercises){
          // Process the succesful request!
          if(exercises){
            vm.exerciseType = exercises;
            for( var i =vm.exerciseType.length - 1; i>=0; i--){
              for( var j=0; j<vm.routine.exercise.length; j++){
                if(vm.exerciseType[i].name === vm.routine.exercise[j].name){
                  vm.exerciseType.splice(i, 1);
                }
              }
            }
          }
        });
      }
    }
    init();

    //Move items functions
    function moveItem(items, from, to){
      items.forEach(function(item){
        var idx = from.indexOf(item);
        if(idx !== -1){
          from.splice(idx, 1);
          to.push(item);
        }
      });
    }

    function moveAll(from, to){
      angular.forEach(from, function(item){ 
        to.push(item);
      });
      from.length = 0;
    }


    // Remove existing Routine
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.routine.$remove($state.go('routines.list'));
      }
    }

    // Save Routine
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.routineForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.routine._id) {
        vm.routine.$update(successCallback, errorCallback);
      } else {
        vm.routine.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('routines.view', {
          routineId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
})();
