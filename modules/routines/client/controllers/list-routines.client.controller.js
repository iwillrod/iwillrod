(function () {
  'use strict';

  angular
    .module('routines')
    .controller('RoutinesListController', RoutinesListController);

  RoutinesListController.$inject = ['RoutinesService'];

  function RoutinesListController(RoutinesService) {
    var vm = this;

    vm.routines = RoutinesService.query();
  }
})();
