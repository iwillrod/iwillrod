//Routines service used to communicate Routines REST endpoints
(function () {
  'use strict';

  angular
    .module('routines')
    .factory('RoutinesService', RoutinesService);

  RoutinesService.$inject = ['$resource'];

  function RoutinesService($resource) {
    return $resource('api/routines/:routineId', {
      routineId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
