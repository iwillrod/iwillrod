(function () {
  'use strict';

  describe('Routines Route Tests', function () {
    // Initialize global variables
    var $scope,
      RoutinesService;

    //We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _RoutinesService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      RoutinesService = _RoutinesService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('routines');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/routines');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          RoutinesController,
          mockRoutine;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('routines.view');
          $templateCache.put('modules/routines/client/views/view-routine.client.view.html', '');

          // create mock Routine
          mockRoutine = new RoutinesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Routine Name'
          });

          //Initialize Controller
          RoutinesController = $controller('RoutinesController as vm', {
            $scope: $scope,
            routineResolve: mockRoutine
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:routineId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.routineResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            routineId: 1
          })).toEqual('/routines/1');
        }));

        it('should attach an Routine to the controller scope', function () {
          expect($scope.vm.routine._id).toBe(mockRoutine._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/routines/client/views/view-routine.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          RoutinesController,
          mockRoutine;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('routines.create');
          $templateCache.put('modules/routines/client/views/form-routine.client.view.html', '');

          // create mock Routine
          mockRoutine = new RoutinesService();

          //Initialize Controller
          RoutinesController = $controller('RoutinesController as vm', {
            $scope: $scope,
            routineResolve: mockRoutine
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.routineResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/routines/create');
        }));

        it('should attach an Routine to the controller scope', function () {
          expect($scope.vm.routine._id).toBe(mockRoutine._id);
          expect($scope.vm.routine._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/routines/client/views/form-routine.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          RoutinesController,
          mockRoutine;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('routines.edit');
          $templateCache.put('modules/routines/client/views/form-routine.client.view.html', '');

          // create mock Routine
          mockRoutine = new RoutinesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Routine Name'
          });

          //Initialize Controller
          RoutinesController = $controller('RoutinesController as vm', {
            $scope: $scope,
            routineResolve: mockRoutine
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:routineId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.routineResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            routineId: 1
          })).toEqual('/routines/1/edit');
        }));

        it('should attach an Routine to the controller scope', function () {
          expect($scope.vm.routine._id).toBe(mockRoutine._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/routines/client/views/form-routine.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
})();
