'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Exercise = mongoose.model('Exercise'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, exercise;

/**
 * Exercise routes tests
 */
describe('Exercise CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Exercise
    user.save(function () {
      exercise = {
        name: 'Exercise name'
      };

      done();
    });
  });

  it('should be able to save a Exercise if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exercise
        agent.post('/api/exercises')
          .send(exercise)
          .expect(200)
          .end(function (exerciseSaveErr, exerciseSaveRes) {
            // Handle Exercise save error
            if (exerciseSaveErr) {
              return done(exerciseSaveErr);
            }

            // Get a list of Exercises
            agent.get('/api/exercises')
              .end(function (exercisesGetErr, exercisesGetRes) {
                // Handle Exercise save error
                if (exercisesGetErr) {
                  return done(exercisesGetErr);
                }

                // Get Exercises list
                var exercises = exercisesGetRes.body;

                // Set assertions
                (exercises[0].user._id).should.equal(userId);
                (exercises[0].name).should.match('Exercise name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Exercise if not logged in', function (done) {
    agent.post('/api/exercises')
      .send(exercise)
      .expect(403)
      .end(function (exerciseSaveErr, exerciseSaveRes) {
        // Call the assertion callback
        done(exerciseSaveErr);
      });
  });

  it('should not be able to save an Exercise if no name is provided', function (done) {
    // Invalidate name field
    exercise.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exercise
        agent.post('/api/exercises')
          .send(exercise)
          .expect(400)
          .end(function (exerciseSaveErr, exerciseSaveRes) {
            // Set message assertion
            (exerciseSaveRes.body.message).should.match('Please fill Exercise name');

            // Handle Exercise save error
            done(exerciseSaveErr);
          });
      });
  });

  it('should be able to update an Exercise if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exercise
        agent.post('/api/exercises')
          .send(exercise)
          .expect(200)
          .end(function (exerciseSaveErr, exerciseSaveRes) {
            // Handle Exercise save error
            if (exerciseSaveErr) {
              return done(exerciseSaveErr);
            }

            // Update Exercise name
            exercise.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Exercise
            agent.put('/api/exercises/' + exerciseSaveRes.body._id)
              .send(exercise)
              .expect(200)
              .end(function (exerciseUpdateErr, exerciseUpdateRes) {
                // Handle Exercise update error
                if (exerciseUpdateErr) {
                  return done(exerciseUpdateErr);
                }

                // Set assertions
                (exerciseUpdateRes.body._id).should.equal(exerciseSaveRes.body._id);
                (exerciseUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Exercises if not signed in', function (done) {
    // Create new Exercise model instance
    var exerciseObj = new Exercise(exercise);

    // Save the exercise
    exerciseObj.save(function () {
      // Request Exercises
      request(app).get('/api/exercises')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Exercise if not signed in', function (done) {
    // Create new Exercise model instance
    var exerciseObj = new Exercise(exercise);

    // Save the Exercise
    exerciseObj.save(function () {
      request(app).get('/api/exercises/' + exerciseObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', exercise.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Exercise with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/exercises/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Exercise is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Exercise which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Exercise
    request(app).get('/api/exercises/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Exercise with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Exercise if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exercise
        agent.post('/api/exercises')
          .send(exercise)
          .expect(200)
          .end(function (exerciseSaveErr, exerciseSaveRes) {
            // Handle Exercise save error
            if (exerciseSaveErr) {
              return done(exerciseSaveErr);
            }

            // Delete an existing Exercise
            agent.delete('/api/exercises/' + exerciseSaveRes.body._id)
              .send(exercise)
              .expect(200)
              .end(function (exerciseDeleteErr, exerciseDeleteRes) {
                // Handle exercise error error
                if (exerciseDeleteErr) {
                  return done(exerciseDeleteErr);
                }

                // Set assertions
                (exerciseDeleteRes.body._id).should.equal(exerciseSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Exercise if not signed in', function (done) {
    // Set Exercise user
    exercise.user = user;

    // Create new Exercise model instance
    var exerciseObj = new Exercise(exercise);

    // Save the Exercise
    exerciseObj.save(function () {
      // Try deleting Exercise
      request(app).delete('/api/exercises/' + exerciseObj._id)
        .expect(403)
        .end(function (exerciseDeleteErr, exerciseDeleteRes) {
          // Set message assertion
          (exerciseDeleteRes.body.message).should.match('User is not authorized');

          // Handle Exercise error error
          done(exerciseDeleteErr);
        });

    });
  });

  it('should be able to get a single Exercise that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Exercise
          agent.post('/api/exercises')
            .send(exercise)
            .expect(200)
            .end(function (exerciseSaveErr, exerciseSaveRes) {
              // Handle Exercise save error
              if (exerciseSaveErr) {
                return done(exerciseSaveErr);
              }

              // Set assertions on new Exercise
              (exerciseSaveRes.body.name).should.equal(exercise.name);
              should.exist(exerciseSaveRes.body.user);
              should.equal(exerciseSaveRes.body.user._id, orphanId);

              // force the Exercise to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Exercise
                    agent.get('/api/exercises/' + exerciseSaveRes.body._id)
                      .expect(200)
                      .end(function (exerciseInfoErr, exerciseInfoRes) {
                        // Handle Exercise error
                        if (exerciseInfoErr) {
                          return done(exerciseInfoErr);
                        }

                        // Set assertions
                        (exerciseInfoRes.body._id).should.equal(exerciseSaveRes.body._id);
                        (exerciseInfoRes.body.name).should.equal(exercise.name);
                        should.equal(exerciseInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Exercise.remove().exec(done);
    });
  });
});
