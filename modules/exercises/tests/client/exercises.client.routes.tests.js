(function () {
  'use strict';

  describe('Exercises Route Tests', function () {
    // Initialize global variables
    var $scope,
      ExercisesService;

    //We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _ExercisesService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      ExercisesService = _ExercisesService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('exercises');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/exercises');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          ExercisesController,
          mockExercise;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('exercises.view');
          $templateCache.put('modules/exercises/client/views/view-exercise.client.view.html', '');

          // create mock Exercise
          mockExercise = new ExercisesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Exercise Name'
          });

          //Initialize Controller
          ExercisesController = $controller('ExercisesController as vm', {
            $scope: $scope,
            exerciseResolve: mockExercise
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:exerciseId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.exerciseResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            exerciseId: 1
          })).toEqual('/exercises/1');
        }));

        it('should attach an Exercise to the controller scope', function () {
          expect($scope.vm.exercise._id).toBe(mockExercise._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/exercises/client/views/view-exercise.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          ExercisesController,
          mockExercise;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('exercises.create');
          $templateCache.put('modules/exercises/client/views/form-exercise.client.view.html', '');

          // create mock Exercise
          mockExercise = new ExercisesService();

          //Initialize Controller
          ExercisesController = $controller('ExercisesController as vm', {
            $scope: $scope,
            exerciseResolve: mockExercise
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.exerciseResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/exercises/create');
        }));

        it('should attach an Exercise to the controller scope', function () {
          expect($scope.vm.exercise._id).toBe(mockExercise._id);
          expect($scope.vm.exercise._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/exercises/client/views/form-exercise.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          ExercisesController,
          mockExercise;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('exercises.edit');
          $templateCache.put('modules/exercises/client/views/form-exercise.client.view.html', '');

          // create mock Exercise
          mockExercise = new ExercisesService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Exercise Name'
          });

          //Initialize Controller
          ExercisesController = $controller('ExercisesController as vm', {
            $scope: $scope,
            exerciseResolve: mockExercise
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:exerciseId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.exerciseResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            exerciseId: 1
          })).toEqual('/exercises/1/edit');
        }));

        it('should attach an Exercise to the controller scope', function () {
          expect($scope.vm.exercise._id).toBe(mockExercise._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/exercises/client/views/form-exercise.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
})();
