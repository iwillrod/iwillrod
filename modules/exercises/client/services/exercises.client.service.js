//Exercises service used to communicate Exercises REST endpoints
(function () {
  'use strict';

  angular
    .module('exercises')
    .factory('ExercisesService', ExercisesService);

  ExercisesService.$inject = ['$resource'];

  function ExercisesService($resource) {
    return $resource('api/exercises/:exerciseId', {
      exerciseId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})();
