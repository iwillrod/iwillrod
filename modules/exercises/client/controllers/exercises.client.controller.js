(function () {
  'use strict';

  // Exercises controller
  angular
    .module('exercises')
    .controller('ExercisesController', ExercisesController);

  ExercisesController.$inject = ['$scope', '$state', 'Authentication', 'exerciseResolve'];

  function ExercisesController ($scope, $state, Authentication, exercise) {
    var vm = this;

    vm.authentication = Authentication;
    vm.exercise = exercise;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.reset = reset;
    vm.selected = selected;

    // Remove existing Exercise
    function remove() {
      if (confirm('Are you sure you want to delete?')) {
        vm.exercise.$remove($state.go('exercises.list'));
      }
    }

    // Save Exercise
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.exerciseForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.exercise._id) {
        vm.exercise.$update(successCallback, errorCallback);
      } else {
        vm.exercise.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('exercises.view', {
          exerciseId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }

    function reset(){
      vm.exercise = {};
      vm.form = angular.copy(vm.exercise);
    }

    function selected(element){
      console.log(element);
    }

    //Execises Type
    vm.exerciseType = [
      'Compound',
      'Isolation'
    ];

    //Add Body Parts
    vm.bodyParts = [
      'Abs',
      'Back',
      'Biceps',
      'Calfs',
      'Chest',
      'Forearms',
      'Hamstrings',
      'Quads', 
      'Shoulders',
      'Triceps'
    ];

  }
})();
