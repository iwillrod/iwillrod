(function () {
  'use strict';

  angular
    .module('exercises')
    .controller('ExercisesListController', ExercisesListController);

  ExercisesListController.$inject = ['ExercisesService'];

  function ExercisesListController(ExercisesService) {
    var vm = this;

    vm.exercises = ExercisesService.query();
  }
})();
