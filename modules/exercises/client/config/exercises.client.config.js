(function () {
  'use strict';

  angular
    .module('exercises')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Exercises',
      state: 'exercises',
      type: 'dropdown',
      roles: ['*'],
      position: 1
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'exercises', {
      title: 'List Exercises',
      state: 'exercises.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'exercises', {
      title: 'Create Exercise',
      state: 'exercises.create',
      roles: ['user']
    });
  }
})();
