(function () {
  'use strict';

  angular
    .module('exercises')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('exercises', {
        abstract: true,
        url: '/exercises',
        template: '<ui-view/>'
      })
      .state('exercises.list', {
        url: '',
        templateUrl: 'modules/exercises/client/views/list-exercises.client.view.html',
        controller: 'ExercisesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Exercises List'
        }
      })
      .state('exercises.create', {
        url: '/create',
        templateUrl: 'modules/exercises/client/views/form-exercise.client.view.html',
        controller: 'ExercisesController',
        controllerAs: 'vm',
        resolve: {
          exerciseResolve: newExercise
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle : 'Exercises Create'
        }
      })
      .state('exercises.edit', {
        url: '/:exerciseId/edit',
        templateUrl: 'modules/exercises/client/views/form-exercise.client.view.html',
        controller: 'ExercisesController',
        controllerAs: 'vm',
        resolve: {
          exerciseResolve: getExercise
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Exercise {{ exerciseResolve.name }}'
        }
      })
      .state('exercises.view', {
        url: '/:exerciseId',
        templateUrl: 'modules/exercises/client/views/view-exercise.client.view.html',
        controller: 'ExercisesController',
        controllerAs: 'vm',
        resolve: {
          exerciseResolve: getExercise
        },
        data:{
          pageTitle: 'Exercise {{ exerciseResolve.name }}'
        }
      });
  }

  getExercise.$inject = ['$stateParams', 'ExercisesService'];

  function getExercise($stateParams, ExercisesService) {
    return ExercisesService.get({
      exerciseId: $stateParams.exerciseId
    }).$promise;
  }

  newExercise.$inject = ['ExercisesService'];

  function newExercise(ExercisesService) {
    return new ExercisesService();
  }
})();
