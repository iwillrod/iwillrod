'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Exercise Schema
 */
var ExerciseSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Exercise name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  numberofsets:{
    type: Number,
    default: 1,
  },
  numberofreps:{
    type: Number,
    default: 1,
  },
  majormuscle:{
    type: String,
    trim: true
  },
  minormuscles:[{
      type: String,
      trime: true
  }],
  workouttype: {
    type: String,
    trim: true
  },
  videoUrl:{
    type: String, 
    trim: true
  },
  explanation:{
    type: String, 
    trim: true
  }, 
  tips:{
    type: String,
    trim: true,
    default: 'No tips this time'
  }
});

mongoose.model('Exercise', ExerciseSchema);
